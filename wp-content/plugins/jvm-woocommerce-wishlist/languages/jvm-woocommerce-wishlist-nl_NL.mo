��          �   %   �      P  1   Q     �     �     �     �     �  p   �     /  -   H  !   v     �     �     �     �     �     �     �     �                    )  C   7  /   {  �   �  �  M  9   +     e     y     �  
   �     �  �   �     )  0   F  0   w     �     �     �     �     �     �     	     	     (	     5	     B	     V	  B   j	  0   �	  �   �	                                                                            	                   
                              A Simple and Lightweight Wishlist for WooCommerce Add to wishlist Cheating huh? Donate In Stock Instructions It is recommended to set your wishlist page here, so themes and plugins can access its URL with the %s function. JVM WooCommerce Wishlist Like this plugin? Please consider a donation. No products on your wishlist yet. Out of Stock Price Product Remove from wishlist Remove this item Return to shop Save Changes Select a page... Settings Wishlist Wishlist Options Wishlist Page You can use the %s shortcode to display your wishlist in your page. Your wishlist page has been created succesfully says : <em>Almost done! you need to <a href="%1$s">create a page</a> for your wishlist or <a href="%2$s">select an existing page</a> in the plugin settings</em>. Project-Id-Version: JVM WooCommerce Wislist
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/jvm-woocommerce-wishlist
POT-Creation-Date: 2019-02-20 09:28:50+00:00
PO-Revision-Date: 2019-02-20 10:54+0100
Last-Translator: Joris van Montfort <jorisvanmontfort@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
 Een eenvoudige lichtgewicht verlanglijst voor WooCommerce Zet op verlanglijst Vals spelen? Doneren Op vooraad Instructies Het wordt aanbevolen om hier uw verlanglijstje in te stellen, zodat thema's en plug-ins toegang hebben tot de URL met de%s functie. JVM WooCommerce Verlanglijst Vind je deze plug-in leuk? Doneer aan de auteur. Er zijn nog geen producten op jouw verlanglijst. Niet op vooraad Prijs Product Verwijder van verlanglijst Dit item verwijderen Terugkeren naar winkel Opslaan Kies een pagina... Instellingen Verlanglijst Verlanglijst Opties Verlanglijst pagina Gebruik de %s shortcode om de verlanglijst op een pagina te tonen. De verlanglijstjepagina is met succes aangemaakt zegt: <em> Bijna klaar! Je moet <a href="%1$s"> een pagina maken </a> voor je verlanglijstje of <a href="%2$s"> een bestaande pagina selecteren </a> in de plugin-instellingen </ em>. 