<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Update callback
 */
add_action( 'jvm_woocommerce_wishlist_do_update', function() {
    // Update runs here
});