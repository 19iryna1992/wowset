<?php /* Template Name: FAQ */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">

    <?php if (have_rows('faqs_repeater')): ?>
        <section class="s-faq">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="s-faq-intro">
                            <h1 class="s-faq-intro__title">
                                <?php echo get_the_title(); ?>
                            </h1>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="c-faq">
                            <?php while (have_rows('faqs_repeater')): the_row();
                                $question = get_sub_field('pytanie');
                                $answer = get_sub_field('odpowiadac');
                                $index = get_row_index();
                                ?>
                                <div class="c-faq__header JS--faq-title <?php echo ($index == 1) ? 'is-active' : null; ?>">
                                    <h2 class="c-faq__title"><?php echo $question; ?></h2>
                                    <span class="c-faq__icon"><?php echo get_accordion_arrow(); ?></span>
                                </div>
                                <div class="c-faq__content JS--faq-content <?php echo ($index == 1) ? 'is-active' : null; ?>">
                                    <?php echo $answer; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

</main>

<?php get_footer(); ?>











