<?php /* Template Name: About Us */

if (!defined('ABSPATH')) exit;

get_header(); ?>

    <main id="main" role="main" tabindex="-1">
        <?php get_template_part('template-parts/sections/quote'); ?>
        <?php get_template_part('template-parts/sections/text-img-repeater'); ?>
        <?php get_template_part('template-parts/sections/instagram'); ?>
        <?php get_template_part('template-parts/sections/videos'); ?>
    </main>

<?php get_footer(); ?>