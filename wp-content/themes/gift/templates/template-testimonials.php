<?php /* Template Name: Testimonials*/

if (!defined('ABSPATH')) exit;

get_header();


?>

    <main id="main" role="main" tabindex="-1">

        <div class="o-testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center"><?php the_title(); ?></h1>
                    </div>
                    <div class="col-12">
                        <?php comments_template(); ?>
                    </div>
                </div>
            </div>
        </div>

    </main>

<?php get_footer(); ?>