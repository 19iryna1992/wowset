<?php /* Template Name: Nowości*/

if (!defined('ABSPATH')) exit;

get_header();

function filter_where( $where = '' ) {
    // за последние 30 дней
    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
    return $where;
}

add_filter( 'posts_where', 'filter_where' );
$new_prod_args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);

$newest_products = new WP_Query($new_prod_args);
remove_filter( 'posts_where', 'filter_where' );
?>

    <main id="main" class="woocommerce" role="main" tabindex="-1">

        <div class="o-products__loop">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php
                        if ($newest_products->have_posts()) {

                            /**
                             * Hook: woocommerce_before_shop_loop.
                             *
                             * @hooked woocommerce_output_all_notices - 10
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action('woocommerce_before_shop_loop');

                            woocommerce_product_loop_start();


                            while ($newest_products->have_posts()) {
                                $newest_products->the_post();

                                /**
                                 * Hook: woocommerce_shop_loop.
                                 */
                                do_action('woocommerce_shop_loop');

                                wc_get_template_part('content', 'product');
                            }


                            woocommerce_product_loop_end();

                            /**
                             * Hook: woocommerce_after_shop_loop.
                             *
                             * @hooked woocommerce_pagination - 10
                             */
                            do_action('woocommerce_after_shop_loop');
                        } else {
                            /**
                             * Hook: woocommerce_no_products_found.
                             *
                             * @hooked wc_no_products_found - 10
                             */
                            do_action('woocommerce_no_products_found');
                        }

                        /**
                         * Hook: woocommerce_after_main_content.
                         *
                         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                         */
                        do_action('woocommerce_after_main_content');

                        /**
                         * Hook: woocommerce_sidebar.
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        do_action('woocommerce_sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>

    </main>

<?php get_footer(); ?>