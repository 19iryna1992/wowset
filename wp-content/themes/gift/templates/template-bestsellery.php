<?php /* Template Name: Bestsellery */

if (!defined('ABSPATH')) exit;

get_header();



$bestseller_args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'meta_key' => 'total_sales',
    'orderby' => 'meta_value_num'

);

$bestseller_products = new WP_Query($bestseller_args);

?>

    <main id="main" class="woocommerce" role="main" tabindex="-1">

        <div class="o-products__loop">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="c-intro-title s-default-page__title text-center"><?php the_title(); ?></h1>
                    </div>
                    <div class="col-12">
                        <?php
                        if ($bestseller_products->have_posts()) {

                            /**
                             * Hook: woocommerce_before_shop_loop.
                             *
                             * @hooked woocommerce_output_all_notices - 10
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action('woocommerce_before_shop_loop');

                            woocommerce_product_loop_start();


                            while ($bestseller_products->have_posts()) {
                                $bestseller_products->the_post();

                                /**
                                 * Hook: woocommerce_shop_loop.
                                 */
                                do_action('woocommerce_shop_loop');

                                wc_get_template_part('content', 'product');
                            }


                            woocommerce_product_loop_end();

                            /**
                             * Hook: woocommerce_after_shop_loop.
                             *
                             * @hooked woocommerce_pagination - 10
                             */
                            do_action('woocommerce_after_shop_loop');
                        } else {
                            /**
                             * Hook: woocommerce_no_products_found.
                             *
                             * @hooked wc_no_products_found - 10
                             */
                            do_action('woocommerce_no_products_found');
                        }

                        /**
                         * Hook: woocommerce_after_main_content.
                         *
                         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                         */
                        do_action('woocommerce_after_main_content');

                        /**
                         * Hook: woocommerce_sidebar.
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        do_action('woocommerce_sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>

    </main>

<?php get_footer(); ?>