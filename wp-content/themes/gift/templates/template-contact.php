<?php /* Template Name: Contact Page */

if (!defined('ABSPATH')) exit;

get_header();

//vars


$form_shortcode = get_field('kontakt_form_shortcode');
?>

    <main id="main" role="main" tabindex="-1">

        <div class="s-contact-page">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-12">
                        <h1 class="c-intro-title text-center"><?php the_title(); ?></h1>
                    </div>
                    <div class="col-md-6 col-lg-4 order-md-2">
                        <div class="s-contact-page__info">
                            <?php get_template_part('template-parts/components/contact-info'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 order-md-1">
                        <div class="s-contact-page__form">
                            <?php get_template_part('template-parts/components/form', null, ['form_title' => '', 'form_shortcode' => $form_shortcode]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main>

<?php get_footer(); ?>