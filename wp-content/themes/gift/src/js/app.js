global.$ = global.jQuery = require('jquery');

require('selectric/src/jquery.selectric');
require('./components/global');
require('./components/faq');
require('./components/sliders');
//require('./components/popups');
require('./components/product');
require('./components/mobile-header');
require('./components/form');
require('./components/filter-accordion');