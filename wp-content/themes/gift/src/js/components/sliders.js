import Swiper from 'swiper';
import {Autoplay} from 'swiper/js/swiper.esm';

$(document).ready(function () {

    //Single image slider
    let heroSlider = $('.JS--main-hero-slider-vertical');

    if (heroSlider) {
        var swiper = new Swiper(heroSlider, {
            effect: "fade",
            fadeEffect: {
                crossFade: true
            },
            autoplay: {
                delay: 3000,
            },
            pagination: {
                el: '.JS--main-hero-slider-pagination',
                clickable: true,
            },
        });
    }

    let iconsSlider = $('.JS--icons-slider');

    if(iconsSlider.length !== 0){
        var iconsSwiper = new Swiper((iconsSlider), {
            slidesPerView: 3,
            simulateTouch:false,
            pagination: {
                   el: '.swiper-pagination',
                   type: 'bullets',
                   clickable:true,
               },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is >= 480px
                768: {
                    slidesPerView: 3,
                    spaceBetween: 26
                },
            }
        })
    }

     //Products Sliders Types
    let productsTypesSlider = $('.JS--products-types-slider');

    if (productsTypesSlider.length !== 0) {

        productsTypesSlider.each(function(){
            var productsSwiper = new Swiper(($(this)[0]), {
                slidesPerView: 4,
                //loop: true,
                /*   pagination: {
                       el: '.swiper-pagination',
                       type: 'bullets',
                       clickable:true,
                   },*/
                navigation: {
                    nextEl: $(this).closest('.JS--products-sliders').find('.JS--related-slider-next'),
                    prevEl: $(this).closest('.JS--products-sliders').find('.JS--related-slider-prev'),
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 8
                    },
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 3,
                        spaceBetween: 20
                    },
                    // when window width is >= 640px
                    1025: {
                        slidesPerView: 4,
                        spaceBetween: 34
                    }
                }
            })
        });

    }


    //Related products slider
    let relatedProductsSlider = $('.JS--related-products-slider');

    if (relatedProductsSlider.length !== 0) {
        var relatedSwiper = new Swiper((relatedProductsSlider), {
            slidesPerView: 4,
            loop: true,
            /*   pagination: {
                   el: '.swiper-pagination',
                   type: 'bullets',
                   clickable:true,
               },*/
            navigation: {
                nextEl: '.JS--related-slider-next',
                prevEl: '.JS--related-slider-prev',
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                // when window width is >= 480px
                480: {
                    slidesPerView: 3,
                    spaceBetween: 26
                },
                // when window width is >= 640px
                1025: {
                    slidesPerView: 4,
                    spaceBetween: 34
                }
            }
        })
    }


});