$(document).ready(function () {
    $('select').selectric();


    let searchForm = $('.JS--search-form');
    let formHolder = $('.JS--search-form-holder');
    let searchOpen = $('.JS--search-open');
    let searchClose = $('.JS--close-search');

    if (formHolder.length !== 0) {

        searchOpen.on('click', function () {
            formHolder.fadeIn(300);
        });

        searchClose.on('click', function () {
            $('.JS--search-input').val('');
            formHolder.fadeOut(300);
        });

        searchForm.on('submit', function (e) {
            let searchInputVal = $(this).find('.JS--search-input').val();
            if (searchInputVal.length) {
            } else {
                e.preventDefault();
            }
        });
    }


    let mc4wpTermsLabel = $('.c-newsletter__terms label');

    mc4wpTermsLabel.on('click', function () {
        let currentInput = $(this).parent().find('input');
        currentInput.prop("checked", !currentInput.prop("checked"));
    });

});

$(window).scroll(function () {
    var sticky = $('.o-header__middle');
    var scroll = $(window).scrollTop();
    if (scroll >= 10) {
        $('body').addClass('has-sticky');
        sticky.addClass('is-sticky');
    } else {
        $('body').removeClass('has-sticky');
        sticky.removeClass('is-sticky');
    }
});


