$(document).ready(function () {


    let options = $('form.cart .extra-options input[type="radio"]');

    let optionsImages = $('.JS--product-hidden-options .JS--add-prod-option');

    if (options.length !== 0) {

        options.each(function () {
            let currentOptionID = $(this).attr('id');
            let optionImage = optionsImages.filter("[data-option-input=" + currentOptionID + "]");

            $(this).parent().append(optionImage);

        });
    }

    let filterBtn = $('.JS--sidebar-filter-btn');
    let productSidebar = $('.JS--product-sidebar');

    if (productSidebar.length) {
        filterBtn.on('click', function () {
            $(this).toggleClass('is-active');
            productSidebar.toggleClass('is-active');
            $('ul.products').toggleClass('filter-opened');
        });
    }

});


