document.addEventListener('DOMContentLoaded', function () {
    const Forms = document.querySelectorAll('.wpcf7-form');
  //  const Form_testimonial = document.querySelectorAll('.wpd-form');


    Forms.forEach(form => {
        let fields = form.querySelectorAll('input,textarea');
        fields.forEach(field => {
            let placeholder = field.getAttribute('placeholder');
            if (placeholder) {
                let spanNode = document.createElement('label');
                let spanTextNode = document.createTextNode(placeholder);
                spanNode.appendChild(spanTextNode);
                field.parentElement.appendChild(spanNode);
                field.setAttribute('placeholder', '');
                field.addEventListener('input', function (e) {
                    let value = e.target.value;
                    if (value) {
                        spanNode.classList.add('has-value');
                    } else {
                        spanNode.classList.remove('has-value');
                    }
                });
            }
        })

    });
});
