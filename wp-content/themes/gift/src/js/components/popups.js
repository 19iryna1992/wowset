require('magnific-popup');
$(document).ready(function () {


    $('.JS--open-pomoc-popup a').magnificPopup({
        type: 'inline',
        closeOnBgClick: true,
        preloader: false,
        fixedContentPos: true,
        fixedBgPos: true,
        midClick: true,
        overflowY: 'auto',
        removalDelay: 300,
        showCloseBtn: false,
        mainClass: 'my-mfp-zoom-in c-form-popup-pomoc',
        callbacks: {
            open: function () {

            },
            close: function () {

            }
        }
    });

    $('.JS--close-popup').on('click', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


});