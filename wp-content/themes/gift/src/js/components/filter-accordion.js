$(document).ready(function () {
    // Accordion

    var accordions = document.getElementsByClassName("JS--btn-accordion");

    for (var i = 0; i < accordions.length; i++) {
        accordions[i].onclick = function () {
            var content = this.nextElementSibling;
            this.classList.toggle('is-open');
            content.classList.toggle('is-close');

        }
    }


});
