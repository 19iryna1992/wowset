$(document).ready(function () {

    let faqTitles = $('.JS--faq-title');

    faqTitles.on('click',function(){
        $(this).siblings('.JS--faq-title').removeClass('is-active');
        $(this).siblings('.JS--faq-content:visible').slideUp();
        $(this).toggleClass('is-active');
        if ($(this).hasClass('is-active')) {
            $(this).next().slideDown();
        }
        $(this).siblings('.JS--faq-title').next('.JS--faq-content').slideUp();
    });


});

