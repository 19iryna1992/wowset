$(document).ready(function () {

    let openMenuBtn = $('.JS-open-mobile-menu');
    let closeMenuBtn = $('.JS--close-mob-menu');
    let mobileMenu = $('.JS--mobile-menu');
    let subMenuBtn = $('.JS--open-sub-menu');

    openMenuBtn.on('click', function () {
        openMenu();
    });

    closeMenuBtn.on('click', function () {
        closeMenu();
    });

    subMenuBtn.on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('is-open');
        let subMenu = $(this).parent().next('.sub-menu');
        subMenu.slideToggle(300);
    });


    function openMenu() {
        mobileMenu.addClass('is-open');
    }

    function closeMenu() {
        mobileMenu.removeClass('is-open');
    }


});