<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gift
 */

get_header();

?>

    <main id="main" role="main" tabindex="-1">

        <section class="s-404 py-3 py-lg-5 yellow-bg d-flex flex-column justify-content-center">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-12">
                        <h1 class="c-section-intro__title c-section-intro__title--large text-center">404</h1>
                    </div>

                </div>
            </div>
        </section>

    </main>

<?php
get_footer(); ?>
