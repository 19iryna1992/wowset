<?php
//vars
$slider_title = get_sub_field('slider_title');
$slider_type = get_sub_field('slider_type');


$product_args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => 8,
);


// start product type cases
if ($slider_type == 'premium') {

    $premium_term_id = apply_filters('wpml_object_id', 61, 'category', TRUE);
    $slider_products = new WP_Query($args = array_merge($product_args, array(
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $premium_term_id,
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            ),
        )
    )));

} elseif ($slider_type == 'best') {
    $slider_products = new WP_Query($args = array_merge($product_args, array('meta_key' => 'total_sales', 'orderby' => 'meta_value_num')));
} elseif ($slider_type == 'sales') {
    $sales_tax = array('meta_query' => array(
        'relation' => 'OR',
        array( // Simple products type
            'key' => '_sale_price',
            'value' => 0,
            'compare' => '>',
            'type' => 'numeric'
        ),
        array( // Variable products type
            'key' => '_min_variation_sale_price',
            'value' => 0,
            'compare' => '>',
            'type' => 'numeric'
        )
    ));
    $slider_products = new WP_Query($args = array_merge($product_args, $sales_tax));
} else {
    $slider_products = new WP_Query($product_args);
}


?>
<?php if ($slider_products->have_posts()) : ?>
    <section class="s-sliders JS--products-sliders woocommerce">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-sliders__header">
                        <h2 class="c-intro-title"><?php echo esc_html($slider_title); ?></h2>
                        <div class="s-sliders__nav">
                            <div class="swiper-button-prev JS--related-slider-prev">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                            <div class="swiper-button-next JS--related-slider-next">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="c-slider">
                        <div class="c-slider__wrapper">
                            <div class="c-slider__slider JS--products-types-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <?php while ($slider_products->have_posts()) : $slider_products->the_post(); ?>

                                        <div class="swiper-slide">
                                            <div class="c-slider__slide c-product-slide">

                                                <!-- <a href="<?php /*the_permalink(); */ ?>">
                                                    <?php /*the_post_thumbnail('medium'); */ ?>
                                                </a>-->
                                                <?php wc_get_template_part('content', 'product'); ?>
                                            </div>

                                        </div>
                                    <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>








