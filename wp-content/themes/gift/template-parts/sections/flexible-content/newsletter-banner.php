<?php
//vars
$title = get_field('newsletter_title', 'option');
$subtitle = get_field('newsletter_subtitle', 'option');
?>
<section class="s-newsletter-banner">
    <div class="s-newsletter-banner__left-decoration">
        <?php echo get_newsletter_left_decor(); ?>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="c-newsletter-wrapper__intro u-white">
                    <?php if ($title) : ?>
                        <h2 class="c-newsletter-wrapper__title"><?php echo $title; ?></h2>
                    <?php endif; ?>
                    <?php if ($subtitle) : ?>
                        <h4 class="c-newsletter-wrapper__subtitle"><?php echo $subtitle; ?></h4>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="s-newsletter-banner__form-wrapper">
                    <div class="s-newsletter-banner__form">
                        <?php get_template_part('template-parts/components/newsletter'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="s-newsletter-banner__right-decoration">
        <?php echo get_newsletter_right_decor(); ?>
    </div>
</section>