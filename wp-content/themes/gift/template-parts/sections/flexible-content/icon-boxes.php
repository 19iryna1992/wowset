<?php if (have_rows('icon_boxes')): ?>
    <section class="s-icon-boxes">
        <div class="s-icon-boxes__left-decoration">
            <?php echo get_left_circle() ?>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container JS--icons-slider">
                        <div class="swiper-wrapper">
                            <?php while (have_rows('icon_boxes')): the_row();
                                $box_label = get_sub_field('icon_text');
                                $icon = get_sub_field('icon_img');
                                ?>
                                <div class="swiper-slide">
                                    <div class="c-icon-box">
                                        <div class="c-icon-box__wrap">
                                            <div class="c-icon-box__header u-primary">
                                                <?php echo wp_get_attachment_image($icon['ID'], 'full') ?>
                                            </div>
                                            <div class="c-icon-box__body">
                                                <div class="c-icon-box__text">
                                                    <?php echo $box_label; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>

            </div>
        </div>



        <div class="s-icon-boxes__right-decoration">
            <?php echo get_right_circle() ?>
        </div>
    </section>
<?php endif; ?>









