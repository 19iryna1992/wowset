<?php
$featured_posts = get_sub_field('relationship_product_slider');
$slider_title= get_sub_field('product_slider_title');
if ($featured_posts): ?>
    <section class="s-sliders JS--products-sliders woocommerce">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-sliders__header">
                        <h2 class="c-intro-title"><?php echo esc_html($slider_title); ?></h2>
                        <div class="s-sliders__nav">
                            <div class="swiper-button-prev JS--related-slider-prev">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                            <div class="swiper-button-next JS--related-slider-next">
                                <?php echo get_slider_arrow(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="c-slider">
                        <div class="c-slider__wrapper">
                            <div class="c-slider__slider JS--products-types-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <?php foreach ($featured_posts as $post):
                                        setup_postdata($post);
                                        ?>
                                        <div class="swiper-slide">
                                            <div class="c-slider__slide c-product-slide">
                                                <?php wc_get_template_part('content', 'product'); ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>