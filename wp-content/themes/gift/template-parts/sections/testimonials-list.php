<?php

$post_id_wpml = apply_filters('wpml_object_id', 266, 'page', TRUE);

$general_comments = get_comments([
    'post_id' => $post_id_wpml,
    'number' => 3
]);

$rating = get_post_meta($post_id_wpml, 'wpdiscuz_post_rating');
$rating_count = get_post_meta($post_id_wpml, 'wpdiscuz_post_rating_count');

?>

<?php if ($general_comments) : ?>
    <section class="s-testimonials-list">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="s-testimonials-list__rating">

                        <div class="s-testimonials-list__rating-range">
                            <span><?php echo $rating[0]; ?></span>
                        </div>

                        <div class="s-testimonials-list__rating-stars">
                            <ul>
                                <?php for ($i = 0; $i <= $rating[0]; $i++) :
                                    $rating_count = $rating[0] - $i;
                                    ?>
                                    <?php if ($rating_count >= 1) : ?>
                                    <li><?php echo get_full_star(); ?></li>
                                <?php elseif ($rating_count == 0.5) : ?>
                                    <li><?php echo get_half_star(); ?></li>
                                <?php endif; ?>
                                <?php endfor; ?>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="woocommerce s-testimonials-list__reviews">
                        <div id="reviews">
                            <div id="comments">
                                <ol class="commentlist">

                                    <?php foreach ($general_comments as $comment) : ?>

                                        <?php if ($comment->comment_approved) : ?>

                                            <li class="review byuser comment-author-admin bypostauthor even thread-even depth-1"
                                                id="li-comment-<?php echo $comment->comment_ID; ?>">

                                                <div id="comment-<?php echo $comment->comment_ID; ?>"
                                                     class="comment_container">
                                                    <div class="comment-text">
                                                        <p class="meta">
                                                            <strong class="woocommerce-review__author"><?php echo $comment->comment_author; ?></strong>
                                                        </p>
                                                        <div class="description">
                                                            <p><?php echo $comment->comment_content; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                </ol>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="s-testimonials-list__btn text-center">
                        <a href="<?php the_permalink($post_id_wpml); ?>"
                           class="c-link"><?php _e('Więcej', 'gf'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>