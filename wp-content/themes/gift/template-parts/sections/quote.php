<?php
$description = get_field('quote_intro_description');
$quote = get_field('quote_content');
$quote_author = get_field('quote_author');
?>

<section class="s-quote-intro">
    <div class="s-quote-intro__cloud-decor s-quote-intro__cloud-decor__left d-none d-lg-block">
        <?php echo get_intro_cloud_left_icon() ?>
    </div>
    <div>
        <div class="s-quote-intro__gift-decor  d-none d-lg-block">
            <?php echo get_intro_gift_icon() ?>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1 class="s-about-intro__title">
                    <?php echo get_the_title(); ?>
                </h1>
            </div>
            <?php if ($description): ?>
                <div class="col-12 col-lg-6 ">
                    <div class="s-quote-intro__description">
                        <?php echo $description ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($quote || $quote_author): ?>
            <div class="row justify-content-md-end">
                <div class="col-lg-5">
                    <div class="s-quote-intro__quote-wrap">
                        <?php if ($quote): ?>
                            <div class="s-quote-intro__quote">
                                <?php echo $quote ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($quote_author): ?>
                            <div class="s-quote-intro__author">
                                <?php echo $quote_author ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="s-quote-intro__cloud-decor s-quote-intro__cloud-decor__right">
        <?php echo get_intro_cloud_rigth_icon() ?>
    </div>
</section>
