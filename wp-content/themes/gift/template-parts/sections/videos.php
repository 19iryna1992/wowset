<?php
$args = array(
    'post_type' => 'videos',
    'posts_per_page' => 3,
);


$videos = new WP_Query($args);
?>
<?php if ($videos->have_posts()) : ?>
    <section class="s-videos">
        <div class="container">
            <div class="row">
                <?php while ($videos->have_posts()) : $videos->the_post();
                    $video = get_field('video');
                    ?>
                    <?php if ($video) : ?>
                        <div class="col-4">
                            <?php echo $video ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
