<?php

$inst_shortcode = get_field('inst_shortcode');
?>

<?php if ($inst_shortcode) : ?>
    <section class="s-instagram">
        <div class="container">
            <div class="row">
                <div class="col-12  col-lg-3">

                </div>
                <div class="col-12  col-lg-9">
                    <?php echo do_shortcode($inst_shortcode); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>