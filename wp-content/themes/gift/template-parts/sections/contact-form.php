<?php

$form_title = get_field('contact_form_title', 'option');
$form_description = get_field('contact_form_description','option');
$form_shortcode = get_field('contact_form_shortcode', 'option');
?>
<?php if ($form_shortcode) : ?>
    <section class="s-contact-form">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12">
                    <div class="s-contact-form__container">
                        <?php
                        get_template_part('template-parts/components/form',
                            null, ['form_title' => $form_title,'form_description'=>$form_description, 'form_shortcode' => $form_shortcode]); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>