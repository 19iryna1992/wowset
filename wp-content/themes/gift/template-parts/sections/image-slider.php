<?php if( have_rows('home_image_slider') ): ?>
<section class="s-image-slider">
    <div class="s-image-slider__wrapper">
        <div class="JS--image-slider swiper-container">
            <div class="swiper-wrapper">
                <?php while( have_rows('home_image_slider') ): the_row();
                $image = get_sub_field('slide_image');
                ?>
                <div class="swiper-slide">
                    <div class="s-image-slider__slide u-bg-img" style="background-image: url(<?php echo $image['url'] ?>)"></div>
                </div>
                <?php endwhile; ?>
            </div>
            <!-- Add Pagination -->
            <div class="JS--slider-image-pagination swiper-pagination"></div>
        </div>
    </div>
</section>
<?php endif; ?>









