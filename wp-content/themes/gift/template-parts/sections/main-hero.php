<?php if (have_rows('home_image_slider')): ?>
    <section class="s-main-hero">
        <div class="swiper-container c-main-slider JS--main-hero-slider-vertical">
            <div class="swiper-wrapper">
                <?php while (have_rows('home_image_slider')): the_row();
                    // ACF VARS
                    $bg_color_first = get_sub_field('hero_slide_color_gradient_first');
                    $bg_color_secondary = get_sub_field('hero_slide_color_gradient_secondary');
                    $title = get_sub_field('hero_slide_title');
                    $description = get_sub_field('hero_slide_description');
                    $link = get_sub_field('hero_slide_link');
                    $image = get_sub_field('hero_slide_image');
                    $bg_label = get_sub_field('hero_slide_bg_label');
                    ?>
                    <div class="swiper-slide"
                         style='background: linear-gradient(104.58deg, <?= $bg_color_first ?> 1.21%, <?= $bg_color_secondary ?> 100%);'>
                        <div class="c-main-slider__slide">
                            <div class="container">
                                <?php if ($bg_label) : ?>
                                    <div class="c-main-slider__bg-label">
                                    <span class="c-main-slider__label u-white">
                                        <?php echo $bg_label ?>
                                    </span>
                                    </div>
                                <?php endif; ?>
                                <div class="row justify-content-center justify-content-lg-around">
                                    <?php if ($image) : ?>
                                        <div class="col-11 col-md-5 order-md-2">
                                            <div class="c-main-slider__img">
                                                <?php echo wp_get_attachment_image($image['ID'], 'full') ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class=" col-11 col-md-6 order-md-1">
                                        <div class="c-main-slider__content">
                                            <?php if ($title) : ?>
                                                <h1 class="c-main-slider__title u-white">
                                                    <?php echo $title ?>
                                                </h1>
                                            <?php endif; ?>
                                            <?php if ($description) : ?>
                                                <div class="c-main-slider__description u-white">
                                                    <?php echo $description ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($link): ?>
                                                <div class="c-main-slider__link">
                                                    <a class="c-link u-white"
                                                       href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-11 col-xl-12  order-md-3">
                                        <div class="c-main-slider__icons">
                                            <?php while (have_rows('hero_slide_social')): the_row();
                                                $soc_icon = get_sub_field('hero_social_icon');
                                                $soc_link = get_sub_field('hero_social_link');
                                                ?>
                                                <a class="c-main-slider__icon" href="<?php $soc_link['url'] ?>">
                                                    <?php echo wp_get_attachment_image($soc_icon['ID'], 'full') ?>
                                                </a>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <!-- Add Pagination -->
            <div class="JS--main-hero-slider-pagination c-main-slider__pagination swiper-pagination"></div>
        </div>
    </section>
<?php endif;  ?>