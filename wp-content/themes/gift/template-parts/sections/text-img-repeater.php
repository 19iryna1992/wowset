<?php if (have_rows('content_repeater')): ?>
    <section class="s-content-repeater">
        <?php while (have_rows('content_repeater')): the_row();
            $type_bg = get_sub_field('type_background');
            $bg_text = get_sub_field('text_bg');
            $image_bg = get_sub_field('section_background');
            $image_position = get_sub_field('image_position');
            $image = get_sub_field('section_image');
            $wysiwyg = get_sub_field('section_content');
            ?>
            <div class="s-content-repeater__container u-bg-img"
                 style='background-image: url("<?= $type_bg == 'image' && $image_bg ? $image_bg['url'] : ' ' ?> ")'>
                <div class="container">
                    <div class="row <?= $image_position !== 'none' ? 'justify-content-between' : 'justify-content-center' ?>">
                        <?php if ($image): ?>
                            <div class="col-12 col-md-5 <?= $image_position == 'right' ? 'order-md-2' : '' ?>">
                                <div class="s-content-repeater__img">
                                    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($wysiwyg): ?>
                            <div class="col-12 position-relative <?= $image_position !== 'none' ? 'col-md-6' : 'col-md-7' ?> <?= $image_position == 'right' ? 'order-md-1' : '' ?>">
                                <div class="d-flex align-items-center h-100">
                                    <div class="c-wysiwyg">
                                        <?php echo $wysiwyg; ?>
                                    </div>
                                </div>
                                <?php if ($type_bg == 'text'): ?>
                                    <div class="s-content-repeater__bg-text">
                                        <?php echo $bg_text ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($type_bg == 'text' && $image_position == 'left'): ?>
                    <div class="s-content-repeater__cloud s-content-repeater__cloud-left ">
                        <?php echo get_duble_cloud_left_icon() ?>
                    </div>
                <?php elseif ($type_bg == 'text' && $image_position == 'right'): ?>
                    <div class="s-content-repeater__cloud s-content-repeater__cloud-right">
                        <?php echo get_duble_cloud_rigth_icon() ?>
                    </div>
                    <div class="s-content-repeater__cloud s-content-repeater__cloud-left">
                        <?php echo get_intro_cloud_left_icon() ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php echo wp_get_attachment_image($image, 'full'); ?>
        <?php endwhile; ?>
    </section>
<?php endif; ?>