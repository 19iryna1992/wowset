<section class="s-search-result woocommerce">
    <div class="c-search-result-wrapper o-products__loop">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $s = get_search_query();
        $args = array(
            's' => $s,
            'paged' => $paged,
        );
        // The Query
        $the_query = new WP_Query($args);

        if ($s) :
            if ($the_query->have_posts()) :
                $result_count = $the_query->found_posts; ?>

           <!--     <div class="c-search-result__heading text-center">
                    <p><?php /*_e('We have found', 'gift'); */?> <?/*= $result_count; */?><?php /*_e(' results', 'gift'); */?></p>
                </div>-->

                <div class="woocommerce-search-loop">

                    <div class="container">


                        <ul class="products columns-4">
                            <?php while ($the_query->have_posts()) :
                                $the_query->the_post();
                                ?>

                                <?php wc_get_template_part('content', 'product'); ?>


                            <?php endwhile;
                            do_action('woocommerce_after_shop_loop');
                            ?>
                        </ul>
                    </div>

                </div>
            <?php else : ?>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="c-search-alert">
                                <h2 class="c-search-alert__heading"><?php _e('Whoops, we haven’t find any results for the query.', 'gift'); ?></h2>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endif; ?>


            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav class="c-search-pagination text-right woocommerce-pagination">
                            <?php

                            echo paginate_links(array(
                                'format' => 'page/%#%',
                                'current' => $paged,
                                'total' => $the_query->max_num_pages,
                                'mid_size' => 2,
                                'prev_text' => __('Prev', 'gf'),
                                'next_text' => __('Next', 'gf'),
                                'type' => 'list'
                            ));
                            ?>
                        </nav>
                    </div>
                </div>
            </div>

        <?php endif; ?>
    </div>
</section>
