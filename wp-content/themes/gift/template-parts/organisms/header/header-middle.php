<?php

if (!defined('ABSPATH')) exit;

$logo_text_img = get_field('main_logo_text_image', 'option');
?>
<div class="o-header__middle <?php echo !is_front_page() ? 'has-bg' : null; ?>">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-auto o-header__middle--mobile">
                <?php get_template_part('template-parts/components/navigation/mobile-btn'); ?>
            </div>
            <div class="col col-xl-auto o-header__logo-col">
                <div class="o-header__logo">
                    <?php the_custom_logo(); ?>
                </div>
            </div>
            <div class="col o-header__desktop-nav">
                <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
            </div>
            <div class="col-auto">
                <?php get_template_part('template-parts/components/navigation/icons-menu'); ?>
            </div>
        </div>
    </div>
    <?php if (is_product() || is_shop() || is_product_category()) : ?>
        <?php get_template_part('template-parts/organisms/header/header-bottom'); ?>
    <?php endif; ?>
    <?php get_template_part('template-parts/components/search-form'); ?>
</div>