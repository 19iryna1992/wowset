<?php

if (!defined('ABSPATH')) exit;

?>
<div class="o-header__bottom <?php echo (is_shop()) ? 'bordered-top' : 'bordered-bottom'; ?>">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php get_template_part('template-parts/components/navigation/shop-menu'); ?>
            </div>
        </div>
    </div>
</div>