<?php

if (!defined('ABSPATH')) exit;

$phone = get_field('main_phone', 'option');
?>
<?php if ($phone) : ?>
    <div class="o-header__top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="o-header__phone">
                        <a href="tel:<?php echo str_replace(' ','',$phone); ?>"><?php echo $phone; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>