<?php

if( have_rows('sections') ):

    while ( have_rows('sections') ) : the_row();

        if( get_row_layout() == 'section_icon_boxes' )
            get_template_part('template-parts/sections/flexible-content/icon-boxes');

        if( get_row_layout() == 'sliders_by_type' )
            get_template_part('template-parts/sections/flexible-content/sliders');

        if( get_row_layout() == 'newsletter_banner' )
            get_template_part('template-parts/sections/flexible-content/newsletter-banner');

        if( get_row_layout() == 'product_slider' )
            get_template_part('template-parts/sections/flexible-content/relationship-slider');

    endwhile;
endif;
?>
