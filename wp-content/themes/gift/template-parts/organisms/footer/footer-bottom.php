<div class="o-footer__bottom">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-auto order-2 order-md-1">
                <div class="o-footer__copyright">
                    <?php echo esc_html('© ' . date('Y') . ' ' . __('Wow Set', 'gift')); ?>
                </div>
            </div>
            <div class="col-12 col-md order-1 order-md-2">
                <?php get_template_part('template-parts/components/navigation/bottom-footer-menu'); ?>
            </div>
        </div>
    </div>
</div>
