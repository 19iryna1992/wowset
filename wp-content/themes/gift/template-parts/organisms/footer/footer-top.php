<div class="o-footer__top">
    <div class="container">
        <div class="row flex-lg-nowrap justify-content-center justify-content-lg-between">
            <div class="col-12 col-lg-auto">
                <?php get_template_part('template-parts/components/footer-pay'); ?>

            </div>
            <div class="col-auto col-md-6  col-lg-auto">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-column-1')) : endif; ?>
            </div>
            <div class="col col-md-6 col-lg-auto">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-column-2')) : endif; ?>
            </div>
            <div class="col-12 col-lg-auto">
                <div class="o-footer__newsletter">
                    <?php get_template_part('template-parts/components/newsletter'); ?>
                </div>
                <?php get_template_part('template-parts/components/navigation/socials-menu'); ?>
            </div>
        </div>
    </div>
</div>
