<?php
if (is_shop()):
    $id = get_option('woocommerce_shop_page_id');
endif;

if (have_rows('shop_hero_slider', $id)): ?>
    <section class="s-shop-hero">
        <div class="swiper-container c-main-slider JS--main-hero-slider-vertical">
            <div class="swiper-wrapper">
                <?php while (have_rows('shop_hero_slider', $id)): the_row();
                    // ACF VARS
                    $bg_color_first = get_sub_field('shop_hero_color_gradient_first', $id);
                    $bg_color_secondary = get_sub_field('shop_hero_color_gradient_secondary', $id);
                    $title = get_sub_field('shop_hero_title', $id);
                    $link = get_sub_field('shop_hero_link', $id);
                    $bg_label = get_sub_field('shop_hero_bg_label', $id);
                    ?>
                    <div class="swiper-slide"
                         style='background: linear-gradient(104.58deg, <?= $bg_color_first ?> 1.21%, <?= $bg_color_secondary ?> 100%);'>
                        <div class="c-main-slider__slide">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-4 order-md-2">
                                        <?php if ($bg_label) : ?>
                                            <div class="c-main-slider__bg-label">
                                                <span class="c-main-slider__label u-white">
                                                    <?php echo $bg_label ?>
                                                </span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-12 col-md-7 order-md-1 d-flex flex-column justify-content-center">
                                        <div class="c-main-slider__content">
                                            <?php if ($title) : ?>
                                                <h1 class="c-main-slider__title u-white">
                                                    <?php echo $title ?>
                                                </h1>
                                            <?php endif; ?>
                                            <?php if ($link): ?>
                                                <a class="c-link u-white"
                                                   href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div><!---->
            <!-- Add Pagination -->
            <div class="JS--main-hero-slider-pagination swiper-pagination"></div>
        </div>
    </section>
<?php endif; ?>