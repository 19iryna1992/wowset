<?php
//vars
$form_shortcode = get_field('newsletter_form_shortcode','option');
?>

<div class="c-newsletter-wrapper">
    <div class="c-newsletter-wrapper__form">
        <?php echo do_shortcode($form_shortcode); ?>
    </div>
</div>