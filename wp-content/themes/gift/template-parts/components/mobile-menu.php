<?php $phone = get_field('main_phone', 'option'); ?>
<div class="c-mobile-menu JS--mobile-menu">
    <div class="c-mobile-menu__container">
        <div class="c-mobile-menu__close JS--close-mob-menu">
            <?php echo get_close_icon(); ?>
        </div>
        <nav class="c-mobile-menu-nav">
            <?php
            wp_nav_menu(array(
                'theme_location' => 'primary-menu',
                'menu_id' => 'primary-menu',
                'container_class' => 'c-main-menu-list__container',
                'menu_class' => 'c-mobile-menu__list',
            ));
            ?>
        </nav>
        <?php /*if ($phone) : */?><!--
            <div class="c-mobile-menu__footer">
                <a class="c-mobile-menu__phone"
                   href="tel:<?php /*echo str_replace(' ', '', $phone); */?>"><?php /*echo $phone; */?></a>
            </div>
        --><?php /*endif; */?>
    </div>
</div>
