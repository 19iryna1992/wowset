<?php
$form_title = $args['form_title'];
$form_description = $args['form_description'];
$form_shortcode = $args['form_shortcode'];
?>

<?php if ($form_shortcode) : ?>
    <div class="c-form">
        <?php if ($form_title) : ?>
            <div class="c-form__intro text-left">
                <h2 class="c-form__title"><?php echo $form_title ?></h2>
                <?php if ($form_description) : ?>
                    <div class="c-form__description"><?php echo $form_description; ?></div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="c-form__wrapper">
            <?php echo do_shortcode($form_shortcode); ?>
        </div>
    </div>
<?php endif; ?>