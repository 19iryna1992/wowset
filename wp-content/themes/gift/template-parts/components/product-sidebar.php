<div class="c-product-sidebar">
    <div class="c-product-sidebar__menus">
        <div class="c-product-sidebar__close-btn d-lg-none">
            <?php echo get_close_icon() ?>
        </div>
        <h3 class="c-product-sidebar__title"> <?php _e('Filtr', 'gift') ?></h3>
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('products-sidebar')) : endif; ?>
    </div>
</div>