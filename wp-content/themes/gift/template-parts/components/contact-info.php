<?php
//vars
$phone = get_field('main_phone', 'option');
$email = get_field('main_email', 'option');
$work_time = get_field('godziny_pracy', 'option');
?>

<div class="c-contact-info">
    <ul class="c-contact-info__list">
        <?php if ($phone) : ?>
            <li class="c-contact-info__item">
                <span class="c-contact-info__icon"><?php echo get_phone_icon(); ?></span>
                <a href="tel:<?php echo str_replace(' ','',$phone); ?>" class="c-contact-info__link"><?php echo $phone; ?></a>
            </li>
        <?php endif; ?>
        <?php if ($email) : ?>
        <li class="c-contact-info__item">
            <span class="c-contact-info__icon"><?php echo get_email_icon(); ?></span>
            <a href="maito:<?php echo $email; ?>" class="c-contact-info__link"><?php echo $email; ?></a>
        </li>
        <?php endif; ?>
        <?php if ($work_time) : ?>
        <li class="c-contact-info__item c-contact-info__item--time">
            <p class="c-contact-info__text"><?php _e('Godziny pracy:', 'gift'); ?></p>
            <p class="c-contact-info__text"><?php echo $work_time; ?></p>
        </li>
        <?php endif; ?>
    </ul>
</div>
