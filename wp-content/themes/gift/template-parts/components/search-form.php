<!-- search -->
<div class="c-search-form JS--search-form-holder">
    <div class="c-search-form__container">
        <form class="c-search-form__form JS--search-form" method="get" action="<?php echo home_url(); ?>" role="search">
            <input class="c-search-form__input JS--search-input" type="search" name="s"
                   value="<?php the_search_query(); ?>"
                   placeholder="<?php _e('Szukaj', 'gift'); ?>">
            <input type="hidden" name="lang" value="<?php echo apply_filters('wpml_current_language', NULL); ?>"/>
            <button class="c-search-form__submit" type="submit" role="button">
                <?php echo get_arrow_right(); ?>
            </button>
            <div class="c-search-form__close JS--close-search">
                <?php echo get_close_icon() ?>
            </div>
        </form>
    </div>
</div>
<!-- /search -->
