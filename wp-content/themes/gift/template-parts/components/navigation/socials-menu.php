<?php if (have_rows('social_menu_list','option')): ?>
    <nav class="c-socials-menu">
        <ul class="c-socials-menu__list">
            <?php while (have_rows('social_menu_list','option')): the_row();
                $icon_class = get_sub_field('icon_class');
                $link = get_sub_field('social_link');
                ?>
                <li class="c-socials-menu__item">
                    <a href="<?php echo $link; ?>" class="c-socials-menu__link">
                        <i class="<?php echo $icon_class; ?>"></i>
                    </a>
                </li>
            <?php endwhile; ?>
        </ul>
    </nav>
<?php endif; ?>









