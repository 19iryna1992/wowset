<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<nav class="c-icons-menu">

    <ul class="c-icons-menu__list <?php  echo is_front_page() ? 'c-icons-menu__list--white' : 'c-icons-menu__list--dark' ?>">

        <li class="c-icons-menu__item c-icons-menu__item--search">
            <a href="javascript:void(0);" class="c-icons-menu__link JS--search-open">
                <span class="c-icons-menu__icon"><?php echo get_search_icon(); ?></span>
            </a>
        </li>

        <li class="c-icons-menu__item">
            <a href="/wishlist/" class="c-icons-menu__link">
                <span class="c-icons-menu__icon"><?php echo get_heart_icon(); ?></span>
            </a>
        </li>

        <li class="c-icons-menu__item">
            <a href="<?php echo wc_customer_edit_account_url(); ?>" class="c-icons-menu__link">
                <span class="c-icons-menu__icon"><?php echo get_account_user_icon(); ?></span>
            </a>
        </li>

        <li class="c-icons-menu__item">
            <div class="c-mini-cart">
                <a class="c-mini-cart__btn" href="<?php echo wc_get_cart_url(); ?>"
                   title="<?php _e('View your shopping basket','gift'); ?>">
                    <span class="c-mini-cart__icon"><?php echo get_shop_cart_icon(); ?></span>
                    <span class="c-mini-cart__count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>

                </a>
                <?php if (!is_cart()) : ?>
                    <div class="c-mini-cart__content">
                        <div class="c-mini-cart__content-wrapper">
                            <div class="widget_shopping_cart_content">
                                <?php woocommerce_mini_cart(); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </li>

    </ul>

</nav>