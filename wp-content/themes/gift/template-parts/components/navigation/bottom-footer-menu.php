<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<?php if (has_nav_menu('footer-bottom-menu')): ?>
    <nav class="c-footer-bottom-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'footer-bottom-menu',
            'menu_id' => 'footer-bottom-menu',
            'container_class' => 'c-footer-bottom-menu__container',
            'menu_class' => 'c-footer-bottom-menu__list',
        ));
        ?>
    </nav>
<?php endif; ?>