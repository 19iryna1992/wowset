<?php

if (!defined('ABSPATH')) {
    exit;
}
?>

<?php if (has_nav_menu('shop-menu')): ?>
    <nav class="c-shop-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'shop-menu',
            'menu_id' => 'shop-menu',
            'container_class' => 'c-shop-menu__container',
            'menu_class' => 'c-shop-menu__list',
        ));
        ?>
    </nav>
<?php endif; ?>