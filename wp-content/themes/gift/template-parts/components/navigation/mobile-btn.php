<div class="o-header__additional-nav">
    <div class="o-header__mob-btn JS-open-mobile-menu">
       <?php echo get_hamburger_icon(); ?>
    </div>
    <a href="javascript:void(0);" class="d-inline-block d-xl-none c-icons-menu__link JS--search-open">
        <span class="c-icons-menu__icon"><?php echo get_search_icon(); ?></span>
    </a>
</div>

