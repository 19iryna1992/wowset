<?php if ((have_rows('pay_methods', 'option'))) : ?>
    <div class="c-footer-pay">
        <div class="c-footer-pay__title">
            <?php _e('Akceptujemy platnośći', 'gift'); ?>
        </div>
        <ul class="c-footer-pay__list">
            <?php while (have_rows('pay_methods', 'option')): the_row();
                $image = get_sub_field('payment_image');
                ?>
                <li class="c-footer-pay__item">
                    <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>
<?php endif; ?>