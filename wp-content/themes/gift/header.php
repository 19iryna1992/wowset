<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gift
 */

if (!defined('ABSPATH')) exit;

$global_tracking_body = (class_exists('ACF')) ? get_field('global_tracking_body', 'options') : null;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
# Global / Page Tracking
if ($global_tracking_body) : echo $global_tracking_body; endif;
?>

<div id="page" class="site">


    <header class="o-header">
        <?php get_template_part('template-parts/organisms/header/header-top'); ?>
        <?php get_template_part('template-parts/organisms/header/header-middle'); ?>
    </header>

    <?php get_template_part('template-parts/components/mobile-menu'); ?>

    <div id="content" class="site-content">
