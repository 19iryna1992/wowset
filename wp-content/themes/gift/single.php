<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gift
 */

if (!defined('ABSPATH')) exit;

get_header();

while (have_posts()) : the_post();


?>

    <main id="main" role="main" tabindex="-1">
        <article>
            <?php if (has_post_thumbnail(get_the_ID())) : ?>
                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full'); ?>
                <img class="c-card-work__overlay" src="<?php echo $image[0]; ?>" />
            <?php endif; ?>
            <section class="post-content">
                <div class="container">
                    <?php the_content(); ?>
                </div>
            </section>

        </article>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>