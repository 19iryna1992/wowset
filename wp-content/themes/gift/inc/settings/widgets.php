<?php

if (!defined('ABSPATH')) exit;

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gf_widgets_init()
{

    register_sidebar(array(
        'name' => esc_html__('Footer Column 1', 'gift'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'gift'),
        'before_widget' => '<section id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Column 2', 'gift'),
        'id' => 'footer-2',
        'description' => esc_html__('Add widgets here.', 'gift'),
        'before_widget' => '<section id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Products Archive', 'gift'),
        'id' => 'products-sidebar',
        'description' => esc_html__('Add widgets here.', 'gift'),
        'before_widget' => '<div id="%1$s" class="JS--filter-accordion c-sidebar %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="JS--btn-accordion c-sidebar__title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'gf_widgets_init');