<?php


/*------------------------------------*\
	Menu hide dropdown class
\*------------------------------------*/

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects($items, $args)
{
    // loop
    foreach ($items as &$item) {
        // vars
        $icon = get_field('hide_sub_menu', $item);
        // append icon
        if ($icon) {
            $item->title .= '<span class="dropdown-link-icon JS--open-sub-menu">' . get_arrow_down_icon() . '</span>';
            //add class
            array_push($item->classes, 'hide-dropdown');
        }
    }
    // return
    return $items;
}
