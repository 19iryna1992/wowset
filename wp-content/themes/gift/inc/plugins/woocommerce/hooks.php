<?php

if (!defined('ABSPATH')) exit;

// Remove sidebar
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

//remove fields from Archive products
//remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

add_filter('woocommerce_layered_nav_count', '__return_false');

//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');


//remove single product elements
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_before_shop_loop_item', 'show_archive_selected_category', 15);

function show_archive_selected_category()
{
    $product = wc_get_product(get_the_ID());
    $premium_category = apply_filters('wpml_object_id', 61, 'category', TRUE);
    if (in_array($premium_category, $product->get_category_ids()) && !is_product_category($premium_category)) {
        echo '<div class="archive-product-image">';
        echo '<h4 class="archive-product-image__cat archive-product-image__cat--premium">' . __('Premium', 'gf') . '</h4>';
    }
}
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 20 );

add_action( 'woocommerce_before_shop_loop_item', 'custom_open_link', 10 );
function custom_open_link() {
	?>
		<a href="<?php the_permalink() ?>">
	<?php
}
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

remove_action( 'woocommerce_after_shop_loop_item', 'jvm_woocommerce_add_to_wishlist', 15 );
add_action('woocommerce_before_shop_loop_item', 'jvm_woocommerce_add_to_wishlist', 15);
/*add_filter( 'jvm_add_to_wishlist_icon_html', function($class) {
    return '';
});*/

add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

function woocommerce_template_loop_product_title()
{
    $product = wc_get_product(get_the_ID());
    $premium_category = apply_filters('wpml_object_id', 61, 'category', TRUE);
    if (in_array($premium_category, $product->get_category_ids()) && !is_product_category($premium_category)) {
        echo '</div>';
    }
    echo '<h2 class="woocommerce-loop-product__title">' . get_the_title() . '</h2>';
}


/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
    function loop_columns()
    {
        return 3; // 3 products per row
    }
}

/**
 * Change number of related products output
 */
function woo_related_products_limit()
{
    global $product;

    $args['posts_per_page'] = 6;
    return $args;
}

add_filter('woocommerce_output_related_products_args', 'gf_related_products_args', 20);
function gf_related_products_args($args)
{
    $args['posts_per_page'] = 8; // 4 related products
    //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter('loop_shop_per_page', 'new_loop_shop_per_page', 20);

function new_loop_shop_per_page($cols)
{
    // $cols contains the current number of products per page based on the value stored on Options –> Reading
    // Return the number of products you wanna show per page.
    $cols = 30;
    return $cols;
}


add_action('woocommerce_single_product_summary', 'change_single_product_ratings', 2);
function change_single_product_ratings()
{
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
    add_action('woocommerce_single_product_summary', 'wc_single_product_ratings', 9);
}

function wc_single_product_ratings()
{
    global $product;

    $rating_count = $product->get_rating_count();

    if ($rating_count >= 0) {
        $review_count = $product->get_review_count();
        $average = $product->get_average_rating();
        $count_html = '<div class="count-rating">' . array_sum($product->get_rating_counts()) . '</div>';
        ?>


        <div class="woocommerce-product-rating">
            <a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                <span class="woocommerce-product-rating__count">
                    <?php echo round($average, 1); ?> /
                <?php echo $review_count; ?>
                </span>

                <span class="container-rating">
                <?php //echo $review_count; ?>
                <div class="star-rating">
                    <?php echo wc_get_rating_html($average, $rating_count); ?>
                </div>
                <?php //echo $count_html; ?>
              <!--  <?php /*if (comments_open()) : */ ?><a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                    (<?php /*printf(_n('%s customer review', '%s customer reviews', $review_count, 'woocommerce'), '<span class="count">' . esc_html($review_count) . '</span>'); */ ?>
                    )</a>--><?php /*endif */ ?>
            </span>
            </a>
        </div>
        <?php
    }
}


add_filter('woocommerce_pagination_args', 'filter_function_name_3670');
function filter_function_name_3670($array)
{
    $array["prev_text"] = __('Poprzednia', 'gf');
    $array["next_text"] = __('Następna', 'gf');

    return $array;
}


function custom_form_field_args($args, $key, $value)
{
    $args['placeholder'] = $args['label'];
    if ($args['id'] == 'billing_address_2') {
        $args['placeholder'] = __('Address 2', 'woocommerce');
    }
    $args['label'] = '';
    return $args;
}

add_filter('woocommerce_form_field_args', 'custom_form_field_args', 10, 3);


/*-------------Remove my account fields--------------------*/

add_filter('woocommerce_account_menu_items', 'gift_remove_my_account_links');
function gift_remove_my_account_links($menu_links)
{

    unset($menu_links['dashboard']);


    //unset( $menu_links['dashboard'] ); // Remove Dashboard
    //unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    //unset( $menu_links['orders'] ); // Remove Orders
    //unset( $menu_links['downloads'] ); // Disable Downloads
    //unset( $menu_links['edit-account'] ); // Remove Account details tab
    //unset( $menu_links['customer-logout'] ); // Remove Logout link

    return $menu_links;

}


add_filter('woocommerce_sale_flash', 'add_percentage_to_sale_badge', 20, 3);
function add_percentage_to_sale_badge($html, $post, $product)
{

    if ($product->is_type('variable')) {
        $percentages = array();

        // Get all variation prices
        $prices = $product->get_variation_prices();

        // Loop through variation prices
        foreach ($prices['price'] as $key => $price) {
            // Only on sale variations
            if ($prices['regular_price'][$key] !== $price) {
                // Calculate and set in the array the percentage for each variation on sale
                $percentages[] = round(100 - (floatval($prices['sale_price'][$key]) / floatval($prices['regular_price'][$key]) * 100));
            }
        }
        // We keep the highest value
        $percentage = max($percentages) . '%';

    } elseif ($product->is_type('grouped')) {
        $percentages = array();

        // Get all variation prices
        $children_ids = $product->get_children();

        // Loop through variation prices
        foreach ($children_ids as $child_id) {
            $child_product = wc_get_product($child_id);

            $regular_price = (float)$child_product->get_regular_price();
            $sale_price = (float)$child_product->get_sale_price();

            if ($sale_price != 0 || !empty($sale_price)) {
                // Calculate and set in the array the percentage for each child on sale
                $percentages[] = round(100 - ($sale_price / $regular_price * 100));
            }
        }
        // We keep the highest value
        $percentage = max($percentages) . '%';

    } else {
        $regular_price = (float)$product->get_regular_price();
        $sale_price = (float)$product->get_sale_price();

        if ($sale_price != 0 || !empty($sale_price)) {
            $percentage = round(100 - ($sale_price / $regular_price * 100)) . '%';
        } else {
            return $html;
        }
    }
    return '<span class="onsale">' . esc_html__('-', 'woocommerce') . ' ' . $percentage . '</span>';
}


//For Archive page add to cart
add_filter('woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text'); // 2.1 +
function woo_archive_custom_cart_button_text()
{
    if (has_term('preorder', 'product_cat', $product->ID)) :
        return __('Pre order Now!', 'gf');
    else:
        return __('Dodaj do koszyka', 'gf');
    endif;
}

add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs)
{
    unset($tabs['additional_information']);   // Remove the additional information tab
    return $tabs;
}

add_filter('woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options');
function ud_update_woo_flexslider_options($options)
{
    $options['controlNav'] = 'thumbnails';
    $options['directionNav'] = true;
    $options['slideshow'] = true;
    $options['animationLoop'] = true;
    return $options;
}


/**
 * Change the breadcrumb separator
 */
add_filter('woocommerce_breadcrumb_defaults', 'gf_change_breadcrumb');
function gf_change_breadcrumb($defaults)
{
    $defaults['delimiter'] = '<span class="breadcrumb-delimiter">/</span>';
    $defaults['wrap_before'] = '<div class="woocommerce-breadcrumb-wrapper"><div class="container"><nav class="woocommerce-breadcrumb">';
    $defaults['wrap_after'] = '</nav></div></div>';
    $defaults['home'] = __('Sklep', 'gf');
    return $defaults;
}

/**
 * Replace the home link URL
 */
add_filter('woocommerce_breadcrumb_home_url', 'gf_breadrumb_home_url');
function gf_breadrumb_home_url()
{
    return wc_get_page_permalink('shop');
}

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product )
{

    // Change In Stock Text
    if ($_product->is_in_stock()) {
        $availability['availability'] = __('Zestaw dostępny', 'woocommerce');
    }
    // Change Out of Stock Text
    if (!$_product->is_in_stock()) {
        $availability['availability'] = __('Zestaw będzie dostępny wkrótce', 'woocommerce');
    }
    return $availability;
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
 */
function custom_woocommerce_product_add_to_cart_text() {
    global $product;

    $product_type = $product->get_type();

    switch ( $product_type ) {
        case 'variable':
            return __( 'Zobacz prezent', 'woocommerce' );
            break;
        default:
            return __( 'Zobacz prezent', 'woocommerce' );
    }

}

//Thank you page

add_filter( 'the_title', 'woo_title_order_received', 10, 2 );

function woo_title_order_received( $title, $id ) {
    if ( function_exists( 'is_order_received_page' ) &&
        is_order_received_page() && get_the_ID() === $id ) {
        $title = __('Dziękujemy za zakupy' , 'woocommerce');
    }
    return $title;
}

/**
 * Remove Woocommerce Select2 - Woocommerce 3.2.1+
 */
/*function woo_dequeue_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );

        wp_dequeue_script( 'selectWoo');
        wp_deregister_script('selectWoo');
    }
}
add_action( 'wp_enqueue_scripts', 'woo_dequeue_select2', 100 );*/