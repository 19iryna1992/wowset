<?php

if (!defined('ABSPATH')) exit;

add_action('init', function () {

    $labels = gf_post_type_labels('Video', 'Video');

    $args = array(
        'description' => __('Video', 'gift'),
        'labels' => $labels,
        'supports' => ['title'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_icon'     => 'dashicons-portfolio',
        'menu_position' => 20,
        'rewrite' => ['slug' => 'videos', 'with_front' => false],
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type'    => 'post',
    );

    register_post_type('videos', $args);
});
