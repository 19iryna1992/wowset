<?php

if (!defined('ABSPATH')) exit;

/**
 * Create Global Elements Menu Item
 */
if (function_exists('acf_add_options_page')) {

	# Site Globals (Parent)
	$site_globals = acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title' 	=> 'Options',
		'icon_url'		=> 'dashicons-admin-generic',
		'redirect' 		=> true
	));

    # Header
    $page_header = acf_add_options_sub_page(array(
        'page_title'  => 'Header',
        'menu_title'  => 'Header',
        'menu_slug'   => 'header',
        'position'    =>  '1',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

	# Newsletter
	$page_newsletter = acf_add_options_sub_page(array(
		'page_title'  => 'Newsletter',
		'menu_title'  => 'Newsletter',
		'menu_slug'   => 'newsletter',
		'position'    =>  '2',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));

    # Contact Form
    $page_contact_form = acf_add_options_sub_page(array(
        'page_title'  => 'Contact Form',
        'menu_title'  => 'Contact Form',
        'menu_slug'   => 'contact_form',
        'position'    =>  '3',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

    # Social Menu
    $page_social_menu = acf_add_options_sub_page(array(
        'page_title'  => 'Social Menu',
        'menu_title'  => 'Social Menu',
        'menu_slug'   => 'social_menu',
        'position'    =>  '4',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

	# JS Tracking
	$page_tracking_scripts = acf_add_options_page(array(
		'page_title'  => 'Tracking Scripts',
		'menu_title'  => 'Tracking Scripts',
		'menu_slug'   => 'tracking',
		'position'    =>  '6',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));

    # Footer fields
	$page_footer_fields = acf_add_options_sub_page(array(
		'page_title'  => 'Footer fields',
		'menu_title'  => 'Footer fields',
		'menu_slug'   => 'footer_fields',
		'position'    =>  '7',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));
}
