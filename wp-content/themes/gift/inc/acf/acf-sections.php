<?php

if (!defined('ABSPATH')) exit;

/**
 * ACF Module Loader Class
 *
 * Autoloads ACF Flexible Content Fields as sections by matching the section name with the file name
 * from within the sections directory.
 *
 * @author       Stephen Scaff
 * @see          template-parts/sections
 * @see          advancedcustomfields.com/add-ons/flexible-content-field
 * @version      1.0
 * @example      ACF_section::render(get_row_section());
 */


class ACF_Sections
{
  /**
   * Path of where the section templates are found,
   * (relative to the theme template directory).
   */
  const MODULE_DIRECTORY = '/template-parts/sections/flexible-content/';

  /**
   * Get section
   *
   * @param  {string} $file
   * @param  {array}  $data
   * @return {string}
   */
  static function get_section($section, $data = null)
  {

    $section_dir = get_template_directory() . self::MODULE_DIRECTORY;
    $section_file = '{{section}}.php';
    $find = ['{{section}}', '_'];
    $replace = [$section, '-'];

    // Locate file matching format
    $located_section_file = str_replace($find[0], $replace[0], $section_file);

    if (file_exists($section_dir . $located_section_file)) {
      include($section_dir . $located_section_file);
      return true;
    } else {

      // Find matching section file
      $located_section_file = str_replace($find, $replace, $section_file);

      if (file_exists($section_dir . $located_section_file)) {
        include($section_dir . $located_section_file);
        return true;
      }
    }

    // If no files can be matched,
    // and WP DEBUG is true: throw a warning.
    if (WP_DEBUG) {
      echo "<pre>ACF_Sections: No section template found for $section.</pre>";
    }

    return false;
  }

  /**
   * Render
   * Main rendering method
   *
   * @param  {string} $section
   * @return {string}
   */
  static function render($section, $data = null)
  {
    return self::get_section($section, $data);
  }
}
