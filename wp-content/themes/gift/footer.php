<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gift
 */

if (!defined('ABSPATH')) exit;

?>

</div><!-- #content -->

<?php if (!is_page_template(array('templates/template-contact.php')) && !is_account_page() && !is_cart() && !is_checkout()) : ?>
    <?php get_template_part('template-parts/sections/contact-form'); ?>
<?php endif; ?>

<?php if (is_front_page()) : ?>
    <?php get_template_part('template-parts/sections/testimonials-list'); ?>
<?php endif; ?>

<footer class="o-footer">
    <?php get_template_part('template-parts/organisms/footer/footer-top'); ?>
    <?php get_template_part('template-parts/organisms/footer/footer-bottom'); ?>
</footer>


</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
