<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if (!defined('ABSPATH')) {
    exit;
}

if ($related_products) : ?>

    <section class="related products JS--products-sliders">
        <?php
        $heading = apply_filters('woocommerce_product_related_products_heading', __('Related products', 'woocommerce'));
        if ($heading) : ?>
            <div class="s-sliders__header">
                <h2 class="c-intro-title mb-xl-0"><?php echo esc_html($heading); ?></h2>
                <div class="s-sliders__nav">
                    <div class="swiper-button-prev JS--related-slider-prev">
                        <?php echo get_slider_arrow(); ?>
                    </div>
                    <div class="swiper-button-next JS--related-slider-next">
                        <?php echo get_slider_arrow(); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <?php //woocommerce_product_loop_start(); ?>
        <div class="c-related c-slider">

            <div class="c-slider__wrapper">
                <div class="c-slider__slider JS--products-types-slider swiper-container">
                    <div class="swiper-wrapper">
                        <?php foreach ($related_products as $related_product) : ?>

                            <?php
                            $post_object = get_post($related_product->get_id());

                            setup_postdata($GLOBALS['post'] =& $post_object); ?>
                            <div class="swiper-slide">
                                <div class="c-slider__slide c-product-slide">
                                    <?php  wc_get_template_part('content', 'product'); ?>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

        </div>


        <?php //woocommerce_product_loop_end(); ?>

    </section>
<?php
endif;

wp_reset_postdata();
