<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
?>

<div class="woocommerce-order s-thankyou">

    <?php
    if ($order) :

        $phone = get_field('main_phone', 'option');
        $email = get_field('main_email', 'option');

        do_action('woocommerce_before_thankyou', $order->get_id());
        ?>

        <?php if ($order->has_status('failed')) : ?>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce'); ?></p>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
            <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"
               class="button pay"><?php esc_html_e('Pay', 'woocommerce'); ?></a>
            <?php if (is_user_logged_in()) : ?>
                <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>"
                   class="button pay"><?php esc_html_e('My account', 'woocommerce'); ?></a>
            <?php endif; ?>
        </p>

    <?php else : ?>
        <div class=" woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
            <strong>
                <p>
                    <?php
                    echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Twoj numer zamówienia to: ', 'woocommerce'), $order)
                        . $order->get_order_number() ?>
                </p>
                <p class="u-primary">
                    <?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Wysłaliśmy potwierdzenie na Twój adres e-mail', 'woocommerce'), $order); ?>
                </p>
            </strong>
        </div>

    <?php endif; ?>
    <div class="s-thankyou__home-link">
        <a class='c-link' href="<?= wc_get_page_permalink( 'shop' )?>"><?php _e('Wróć do sklepu', 'gf'); ?></a>
    </div>

    <div class="s-thankyou__contact-info">
        <div class="s-thankyou__link-wrap">
            <a class='c-link--secondary' href=""><?= $phone ?></a>
        </div>
        <div class="s-thankyou__link-wrap">
            <a class='c-link--secondary' href=""><?= $email ?></a>
        </div>
    </div>

        <?php /*do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); */?>
        <?php /*do_action('woocommerce_thankyou', $order->get_id()); */?>

    <?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'woocommerce'), null); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

    <?php endif; ?>

</div>
